/*
$( "#header-plugin" ).load( "https://vivinantony.github.io/header-plugin/", function() {
	$("a.back-to-link").attr("href", "http://blog.thelittletechie.com/2015/03/love-heart-animation-using-css3.html#tlt")  
});
*/
var love = setInterval(function() {
    var r_num = Math.floor(Math.random() * 40) + 1;
    var r_size = Math.floor(Math.random() * 65) + 10;
    var r_left = Math.floor(Math.random() * 100) + 1;
    var r_bg = Math.floor(Math.random() * 25) + 100;
    var r_time = Math.floor(Math.random() * 5) + 5;

    $('.bg_heart').append("<div class='heart adicional' style='width:" + r_size + "px;height:" + r_size + "px;left:" + r_left + "%;background:rgba(255," + (r_bg - 25) + "," + r_bg + ",1);-webkit-animation:love " + r_time + "s ease;-moz-animation:love " + r_time + "s ease;-ms-animation:love " + r_time + "s ease;animation:love " + r_time + "s ease'></div>");

    $('.bg_heart').append("<div class='heart adicional' style='width:" + (r_size - 10) + "px;height:" + (r_size - 10) + "px;left:" + (r_left + r_num) + "%;background:rgba(255," + (r_bg - 25) + "," + (r_bg + 25) + ",1);-webkit-animation:love " + (r_time + 5) + "s ease;-moz-animation:love " + (r_time + 5) + "s ease;-ms-animation:love " + (r_time + 5) + "s ease;animation:love " + (r_time + 5) + "s ease'></div>");

    $('.heart').each(function() {
        var top = $(this).css("top").replace(/[^-\d\.]/g, '');
        var width = $(this).css("width").replace(/[^-\d\.]/g, '');
        if (top <= -100 || width >= 150) {
            $(this).detach();
        }
    });
    $('.adicional').on("click",tocoCorazon);
    $(".adicional").removeClass("adicional");
}, 500);
var arrayFrases = [
    {mensaje:"La sonrisa es mía. Pero el motivo eres tu.",foto:"1"},
    {mensaje:"Cuando estemos viejitos te dire. Veee que si eras el amor de mi vida",foto:"2"},
    {mensaje:"Tienes 3 opciones: Te beso, me besas o nos besamos",foto:"3"},
]

var pos = 0;
function tocoCorazon(){

    document.getElementById("audioMensaje").play();
    var indice = pos%arrayFrases.length;
    frase = arrayFrases[indice];
    pos++;
    console.log("indd",indice,frase);
    $("#imagen").attr({src:"img/"+frase.foto+".jpg"})
    
    $("#contenedorMensaje").fadeIn("slow",function(){
        $("#lblMensaje").html(frase.mensaje);
        animarLetras();
        $("#lblMensaje").css({opacity:1});
    });
    
}
$("#btnSalir").on("click",function(){
    document.getElementById("audioSalida").play();
    $("#contenedorMensaje").fadeOut("slow",function(){
        $("#lblMensaje").css({opacity:0});
        $("#lblMensaje").html("");
    });
})
function animarLetras(){
    /*
    $('.ml2').each(function(){
      $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
    });
    */
    var html = $('.ml2').text();
    var ret  = "";

    $.each(html.split(''), function(k, v) {
       ret += "<span class='letter'>" + v + "</span>";
    });

    $('.ml2').html(ret);
        /*
    $('.ml3').each(function(){
      $(this).html($(this).text().replace(/[a-z\u00C0-\u00F6\u00F8-\017E]/gi, "<span class='letter'>$&</span>"));
    });

    
      */
    animadorLetras = anime.timeline({loop: false})
      .add({
        targets: '.ml2 .letter',
        opacity: [0,1],
        easing: "easeInOutQuad",
        duration: 250,
        delay: function(el, i) {
          return 50 * (i+1)
        }
      });
     animadorLetras.complete = function(){
        console.log("completo animacion");
         //setTimeout(salida,1000);
     }
}
function init(){
    var inicio = false;
    $("body").on("click",function(){
        if(inicio){
            return;
        }
        inicio = true;
        myAudio.play();
    })
    var myAudio = new Audio('sonidos/fondo.mp3'); 
    myAudio.addEventListener('ended', function() {
        this.currentTime = 0;
        this.play();
    }, false);
    myAudio.volume = 0.2;
    //myAudio.play();
}
init();